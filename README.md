Identifies a set cheat code among a given sequence of input in a game of Konami with a board of any size while correctly calculating treasure and enemy positions and executing motions accordingly. 
//written as a project for CSE250 at UB
//part of skeletal code provided by Dr Jesse Hartloff to outline the project
//skelelat-code-logic directory authored mostly by Dr Hartloff