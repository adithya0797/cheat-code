//
// Created by adithya on 2/19/16.
//
//includes Skeleton code provided by Dr Jesse Hartloff to outline the project
// Created by adithya on 2/18/16.
//

#include "TreasureHunter.h"
#include <cmath>

TreasureHunter::TreasureHunter(int x, int y, GameBoard &board) {
    this->x = x;
    this->y = y;
   this->board = &board;
    score = 50;
}

TreasureHunter::~TreasureHunter() {
    // TODO
}

void TreasureHunter::changeBoard(GameBoard &newBoard) {
    // TODO
    board = &newBoard;
}

void TreasureHunter::changeBoard(GameBoard *newBoard) {
    // TODO
    board = newBoard;
}

int TreasureHunter::computeScore(std::string inputString) {

    std::string s = inputString;
    //std::string code = "uuddlrlrba";
    Location Hlocation = Location(x, y);
    Location goal = board->getGoalLocation();
    if (containsKonamiCode(s)) {
        for (int i = 0; i < indexer(s); i++) {
//            if (board->isEnemy(Hlocation)) {
//                if (s[i] == 'u' || s[i] == 'l' || s[i] == 'd' || s[i] == 'r' || s[i] == 'b') { }
//                else if (s[i] == 'a') { board->killEnemy(Hlocation); }
//                else { score = score - 1; }
//            }

            if (s[i] == 'u') {
                if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                }
                if (!board->isEnemy(Hlocation)) {
                    int previous = abs(Hlocation.getY() - goal.getY());
                    Hlocation.setY(Hlocation.getY() + 1);
                    int current = abs(Hlocation.getY() - goal.getY());
                    if (current > previous) {
                        score = score - 2;
                    }
                    else {
                        score = score + 1;
                    }
                }

            }
            else if (s[i] == 'd') {
                if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                }
                if (!board->isEnemy(Hlocation)) {
                    int previous = abs(Hlocation.getY() - goal.getY());
                    Hlocation.setY(Hlocation.getY() - 1);
                    int current = abs(Hlocation.getY() - goal.getY());
                    if (current > previous) {
                        score = score - 2;
                    }
                    else {
                        score = score + 1;
                    }
                }

            }
            else if (s[i] == 'l') {
                if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                }
                if (!board->isEnemy(Hlocation)) {
                    int previous = abs(Hlocation.getX() - goal.getX());
                    Hlocation.setX(Hlocation.getX() - 1);
                    int current = abs(Hlocation.getX() - goal.getX());
                    if (current > previous) {
                        score = score - 2;
                    }
                    else {
                        score = score + 1;
                    }
                }
            }
            else if (s[i] == 'r') {
                if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                }
                if (!board->isEnemy(Hlocation)) {
                    int previous = abs(Hlocation.getX() - goal.getX());
                    Hlocation.setX(Hlocation.getX() + 1);
                    int current = abs(Hlocation.getX() - goal.getX());
                    if (current > previous) {
                        score = score - 2;
                    }
                    else {
                        score = score + 1;
                    }
                }
            }
            else if (s[i] == 'b') {if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                } }
            else if (s[i] == 'a') {
                if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                }
                if (board->isEnemy(Hlocation)) {
                    board->killEnemy(Hlocation);
                }
            }
            else {
                if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                }
                score = score - 1;
            }
//            if (board->isTreasure(Hlocation)) {
//                score = score + 10;
//                board->removeTreasure(Hlocation);
//            }
            if (Hlocation == goal) {
                score = score + 100;
                x = Hlocation.getX();
                y = Hlocation.getY();
                return score;// = score+100;
            }
//            if (board->isEnemy(Hlocation)) {
//                if (s[i] == 'u' || s[i] == 'l' || s[i] == 'd' || s[i] == 'r' || s[i] == 'b') { }
//                else if (s[i] == 'a') { board->killEnemy(Hlocation); }
//                else { score = score - 1; }
//            }

            if (score <= 0) {
                x = Hlocation.getX();
                y = Hlocation.getY();
                return score;
            }
            std::cout << "the score is" << score << "the char is " << s[i] << endl;
            std::cout << "coordinates" << Hlocation.getX() << "  " << Hlocation.getY();

        }
        for (int i = indexer(s); i < indexer(s) + 10; i++) {
            //what to do?
            if (board->isEnemy(Hlocation)) {
                // i = indexer(s) + 11;
                board->killEnemy(Hlocation);
                break;
            }

            if (s[i] == 'u') {
                if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                }
                if (!board->isEnemy(Hlocation)) {
                    int previous = abs(Hlocation.getY() - goal.getY());
                    Hlocation.setY(Hlocation.getY() + 1);
                    int current = abs(Hlocation.getY() - goal.getY());
                    if (current > previous) {
                        score = score - 2;
                    }
                    else {
                        score = score + 1;
                    }
                }
            }
            else if (s[i] == 'd') {
                if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                }
                if (!board->isEnemy(Hlocation)) {
                    int previous = abs(Hlocation.getY() - goal.getY());
                    Hlocation.setY(Hlocation.getY() - 1);
                    int current = abs(Hlocation.getY() - goal.getY());
                    if (current > previous) {
                        score = score - 2;
                    }
                    else {
                        score = score + 1;
                    }
                }
            }
            else if (s[i] == 'l') {
                if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                }
                if (!board->isEnemy(Hlocation)) {
                    int previous = abs(Hlocation.getX() - goal.getX());
                    Hlocation.setX(Hlocation.getX() - 1);
                    int current = abs(Hlocation.getX() - goal.getX());
                    if (current > previous) {
                        score = score - 2;
                    }
                    else {
                        score = score + 1;
                    }
                }
            }
            else if (s[i] == 'r') {
                if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                }
                if (!board->isEnemy(Hlocation)) {
                    int previous = abs(Hlocation.getX() - goal.getX());
                    Hlocation.setX(Hlocation.getX() + 1);
                    int current = abs(Hlocation.getX() - goal.getX());
                    if (current > previous) {
                        score = score - 2;
                    }
                    else {
                        score = score + 1;
                    }
                }
            }
            else if (s[i] == 'b') {if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                } }
            else if (s[i] == 'a') {
                if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                }
                if (board->isEnemy(Hlocation)) {
                    board->killEnemy(Hlocation);
                }
            }
            //else if(s[i])
//            if (board->isTreasure(Hlocation)) {
//                score = score + 10;
//                board->removeTreasure(Hlocation);
//            }
            if (Hlocation == goal) {
                score = score + 100;
                x = Hlocation.getX();
                y = Hlocation.getY();
                return score;// = score+100;
            }
            if (score <= 0) {
                x = Hlocation.getX();
                y = Hlocation.getY();
                return score;
            }
            std::cout << "the score is" << score << "the char is " << s[i] << endl;
            std::cout << "coordinates" << Hlocation.getX() << "  " << Hlocation.getY();
        }
        if (indexer(s) + 10 < s.length()) {
            for (int i = indexer(s) + 10; i < s.length(); i++) {//


                if (s[i] == 'u') {
                    if (board->isTreasure(Hlocation)) {
                        score = score + 10;
                        board->removeTreasure(Hlocation);
                    }
                    if (board->isEnemy(Hlocation)) {
                        board->killEnemy(Hlocation);
                    }
                    int previous = abs(Hlocation.getY() - goal.getY());
                    Hlocation.setY(Hlocation.getY() + 1);
                    int current = abs(Hlocation.getY() - goal.getY());
                    if (current > previous) {
                        score = score - 2;
                    }
                    else {
                        score = score + 1;
                    }

                }
                else if (s[i] == 'd') {
                    if (board->isTreasure(Hlocation)) {
                        score = score + 10;
                        board->removeTreasure(Hlocation);
                    }
                    if (board->isEnemy(Hlocation)) {
                        board->killEnemy(Hlocation);
                    }
                    int previous = abs(Hlocation.getY() - goal.getY());
                    Hlocation.setY(Hlocation.getY() - 1);
                    int current = abs(Hlocation.getY() - goal.getY());
                    if (current > previous) {
                        score = score - 2;
                    }
                    else {
                        score = score + 1;
                    }

                }
                else if (s[i] == 'l') {
                    if (board->isTreasure(Hlocation)) {
                        score = score + 10;
                        board->removeTreasure(Hlocation);
                    }
                    if (board->isEnemy(Hlocation)) {
                        board->killEnemy(Hlocation);
                    }
                    int previous = abs(Hlocation.getX() - goal.getX());
                    Hlocation.setX(Hlocation.getX() - 1);
                    int current = abs(Hlocation.getX() - goal.getX());
                    if (current > previous) {
                        score = score - 2;
                    }
                    else {
                        score = score + 1;
                    }

                }
                else if (s[i] == 'r') {
                    if (board->isTreasure(Hlocation)) {
                        score = score + 10;
                        board->removeTreasure(Hlocation);
                    }
                    if (board->isEnemy(Hlocation)) {
                        board->killEnemy(Hlocation);
                    }
                    int previous = abs(Hlocation.getX() - goal.getX());
                    Hlocation.setX(Hlocation.getX() + 1);
                    int current = abs(Hlocation.getX() - goal.getX());
                    if (current > previous) {
                        score = score - 2;
                    }
                    else {
                        score = score + 1;
                    }

                }
                else if (s[i] == 'b' or s[i] == 'a') {if (board->isTreasure(Hlocation)) {
                        score = score + 10;
                        board->removeTreasure(Hlocation);
                    } }
                else {
                    score = score - 1;
                }
//                if (board->isTreasure(Hlocation)) {
//                    score = score + 10;
//                    board->removeTreasure(Hlocation);
//                }
                if (Hlocation == goal) {
                    score = score + 100;
                    x = Hlocation.getX();
                    y = Hlocation.getY();
                    return score;
                }
                if (score <= 0) {
                    x = Hlocation.getX();
                    y = Hlocation.getY();
                    return score;
                }

                std::cout << "the score is" << score << "the char is " << s[i] << endl;
                std::cout << "coordinates" << Hlocation.getX() << "  " << Hlocation.getY();
            }

        }
    }
    else {
        for (int i = 0; i < s.length(); i++) {
//            if (board->isEnemy(Hlocation)) {
//                if (s[i] == 'u' || s[i] == 'l' || s[i] == 'd' || s[i] == 'r' || s[i] == 'b') { }
//                else if (s[i] == 'a') { board->killEnemy(Hlocation); }
//                else { score = score - 1; }
//            }

            if (s[i] == 'u') {
                if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                }
                if (!board->isEnemy(Hlocation)) {
                    int previous = abs(Hlocation.getY() - goal.getY());
                    Hlocation.setY(Hlocation.getY() + 1);
                    int current = abs(Hlocation.getY() - goal.getY());
                    if (current > previous) {
                        score = score - 2;
                    }
                    else {
                        score = score + 1;
                    }
                }
            }
            else if (s[i] == 'd') {
                if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                }
                if (!board->isEnemy(Hlocation)) {
                    int previous = abs(Hlocation.getY() - goal.getY());
                    Hlocation.setY(Hlocation.getY() - 1);
                    int current = abs(Hlocation.getY() - goal.getY());
                    if (current > previous) {
                        score = score - 2;
                    }
                    else {
                        score = score + 1;
                    }
                }

            }
            else if (s[i] == 'l') {
                if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                }
                if (!board->isEnemy(Hlocation)) {
                    int previous = abs(Hlocation.getX() - goal.getX());
                    Hlocation.setX(Hlocation.getX() - 1);
                    int current = abs(Hlocation.getX() - goal.getX());
                    if (current > previous) {
                        score = score - 2;
                    }
                    else {
                        score = score + 1;
                    }
                }
            }
            else if (s[i] == 'r') {
                if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                }
                if (!board->isEnemy(Hlocation)) {
                    int previous = abs(Hlocation.getX() - goal.getX());
                    Hlocation.setX(Hlocation.getX() + 1);
                    int current = abs(Hlocation.getX() - goal.getX());
                    if (current > previous) {
                        score = score - 2;
                    }
                    else {
                        score = score + 1;
                    }
                }
            }
            else if (s[i] == 'b') {
                if (board->isTreasure(Hlocation)) {
                    score = score + 10;
                    board->removeTreasure(Hlocation);
                }}
            else if (s[i] == 'a') {
                if (board->isEnemy(Hlocation)) {
                    board->killEnemy(Hlocation);
                }
            }
            else {
                score = score - 1;
            }

//            if (board->isTreasure(Hlocation)) {
//                score = score + 10;
//                board->removeTreasure(Hlocation);
//            }
            if (Hlocation == goal) {
                score = score + 100;
                x = Hlocation.getX();
                y = Hlocation.getY();
                return score;// = score+100;
            }
            if (score <= 0) {
                x = Hlocation.getX();
                y = Hlocation.getY();
                return score;
            }
            std::cout << "the score is" << score << "the char is " << s[i] << endl;
            std::cout << "coordinates" << Hlocation.getX() << "  " << Hlocation.getY();
        }
    }



    x = Hlocation.getX();y = Hlocation.getY();
    return score;


}


int TreasureHunter::indexer(std::string inputString) {
    return inputString.find("uuddlrlrba");
}
